config = {
	borderless = "false",
	facebook = "http://facebook.com/moosader",
	fullscreen = "false",
	language = "english",
	music_volume = "100",
	savegame_count = "",
	screen_height = "720",
	screen_width = "1280",
	sound_volume = "67",
	twitter = "http://twitter.com/moosader",
	vsync = "true",
	website = "http://www.moosader.com",
}
