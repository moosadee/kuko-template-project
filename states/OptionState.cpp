#include "OptionState.hpp"
#include <cstdio>
#include <cstdlib>

#include "../kuko/managers/ImageManager.hpp"
#include "../kuko/managers/InputManager.hpp"
#include "../kuko/managers/FontManager.hpp"
#include "../kuko/managers/LanguageManager.hpp"
#include "../kuko/managers/SoundManager.hpp"
#include "../kuko/managers/ConfigManager.hpp"
#include "../kuko/managers/LuaManager.hpp"
#include "../kuko/utilities/Logger.hpp"
#include "../kuko/base/Application.hpp"
#include "../kuko/utilities/StringUtil.hpp"
#include "../kuko/utilities/Platform.hpp"
#include "../kuko/utilities/Messager.hpp"

OptionState::~OptionState()
{
}

OptionState::OptionState( const std::string& contentPath ) : IState()
{
    SetContentPath( contentPath );
}

void OptionState::Setup()
{
    m_state = "";
    kuko::IState::Setup();
    //kuko::SoundManager::PlayMusic( "title", true );

    Logger::Debug( "Menu Transition", "OptionState::SetupMenu_OptionState" );
    menuManager.LoadMenuScript( m_contentPath + "menus/option_screen.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();

    // Set the sliders to match the config volume
    AdjustVolume( "" );
}

void OptionState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
}

void OptionState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

void OptionState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::WINDOW_CLOSE ].down )
    {
        m_gotoState = "quit";
    }
    else if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX, y = input[ kuko::TAP ].actionY;
        std::string clickedButtonId = menuManager.GetButtonClicked( x, y );

        if ( clickedButtonId == "btn_back" )
        {
            kuko::SoundManager::PlaySound( "button_confirm" );
            m_gotoState = "title";
        }
        else if ( clickedButtonId.find( "plus" ) != std::string::npos || clickedButtonId.find( "minus" ) != std::string::npos )
        {
            AdjustVolume( clickedButtonId );
        }
    }

    menuManager.Update();
}

void OptionState::Draw()
{
    menuManager.Draw();
}

void OptionState::AdjustVolume( const std::string& buttonName )
{
    int music = StringUtil::StringToInt( kuko::ConfigManager::GetOption( "music_volume" ) );
    int sound = StringUtil::StringToInt( kuko::ConfigManager::GetOption( "sound_volume" ) );

    if ( buttonName == "btn_minus_music" )
    {
        music -= 33;
    }
    else if ( buttonName == "btn_plus_music" )
    {
        music += 33;
    }

    if ( buttonName == "btn_minus_sound" )
    {
        sound -= 33;
    }
    else if ( buttonName == "btn_plus_sound" )
    {
        sound += 33;
    }

    if ( music > 100 ) { music = 100; }
    else if ( music < 0 ) { music = 0; }
    if ( sound > 100 ) { sound = 100; }
    else if ( sound < 0 ) { sound = 0; }

    // Update config
    kuko::ConfigManager::SetOption( "music_volume", StringUtil::IntToString( music ) );
    kuko::ConfigManager::SetOption( "sound_volume", StringUtil::IntToString( sound ) );
    kuko::ConfigManager::SaveConfig();

    kuko::SoundManager::SetMusicVolume( music );
    kuko::SoundManager::SetSoundVolume( sound );

    // Update graphics
    music /= 33;
    sound /= 33;

    menuManager.GetButton( "sound_slider" )->SetFrame( kuko::IntRect( 0, sound * 40, 294, 40 ) );
    menuManager.GetButton( "music_slider" )->SetFrame( kuko::IntRect( 0, music * 40, 294, 40 ) );

    kuko::SoundManager::PlaySound( "button_confirm" );
}
