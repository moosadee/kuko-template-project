#include "KukoApp.hpp"

int main( int argc, char* args[] )
{
    KukoApp app;
    app.Run();

    return 0;
}
